# <div align="center">Mark Harter </div>
<div align="center">markharter05@gmail.com  / 0411423118  /  linkedin.com/in/mark-harter </div>

## Summary 

Experienced professional with a background in both IT and retail. A motivated and organised individual with strong collaboration, multitasking, and customer-centric abilities. Seeks entry-level opportunities to contribute to company growth while applying acquired skills and expanding knowledge. 

## Qualifications

Bachelor's degree, (Cyber security and Behaviour), Western Sydney University, 2022

Certificate III, (Retailing and Retail Operations) MCDONALD'S AUSTRALIA LTD, 2021 

Advanced Diploma , (Network Security) TAFE NSW, 2017

## Experience 

### **Shift Assistant**, McDonald's  , Aug 2023 - Present  ###

- Solve customer complaints
    
- Assisting shift managers run shift
    
- Leading the shift to successfully achieve daily KPI’s
    

### **Crew Coach**, McDonald's ,Jul 2017 - Aug 2023 ###

- Successfully delivered required training sessions to new hires, emphasising the importance of exceptional customer service.
    
- Continuously reviewing and updating policies based on feedback, maintaining compliance through updated training initiatives.
    
- Sustained a high level of customer satisfaction and loyalty through consistent delivery of gold standard customer service.
    

### **Infrastructure Manager**, Western Center for Cyber Aid and Community Engagement (WCACE) , May 2022 - Present ###

- Continuously updated training material based on emerging threats for example methods on mitigating phishing campaigns and industry leading standards to ensure up-to-date knowledge among employees.
    
- Led a high-performing SOC team, achieving a 30% reduction in incident resolution time.
    
- Lead a team who utilised strong problem-solving skills and knowledge of cyber security protocols to provide timely and effective solutions.
    
- Deployed and configured Linux-based desktops, implementing standardised configurations and security measures.**